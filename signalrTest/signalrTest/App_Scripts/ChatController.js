﻿app.controller('chatController', ['$scope', 'chatService', function ($scope, chatService) {

    $scope.messages = [];
    $scope.username = '';
    $scope.newMessage = '';

    /// Event handler for when the SignlaR Hub is initialized.
    $scope.$on('raiseHubInitializationComplete', function (event) {
        // hub is setup now.
    });

    $scope.$on('addNewMessageToPageResponse', function (event, message) {
        $scope.$apply(function () {
            $scope.messages.push(message);
        });
    });

    $scope.send = function () {
        chatService.sendMessage($scope.newMessage);
    };

    /// Initialize the Service.  This will start the SignalR Hub.
    chatService.initializeService('chatHub');
}]);

app.controller('chatController2', ['$scope','$http', 'chatService', function ($scope, $http, chatService) {
    $scope.messages = [];
    $scope.username = '';
    $scope.newMessage = '';

    /// Event handler for when the SignlaR Hub is initialized.
    $scope.$on('raiseHubInitializationComplete', function (event) {
        // hub is setup now.
    });

    $scope.$on('addNewMessageToPageResponse', function (event, chatMessage) {
        $scope.$apply(function () {
            $scope.messages.push({user: chatMessage.UserName, message: chatMessage.Message});
        });
    });

    $scope.send = function () {
        $http({ method: 'POST', url: rootUrl + 'api/chat', data: {UserName: $scope.username, Message: $scope.newMessage}});
    };

    /// Initialize the Service.  This will start the SignalR Hub.
    chatService.initializeService('chatting');
}]);