﻿app.factory('chatService', ['$rootScope', '$http', 'signalrChatServiceBase', function ($rootScope, $http, signalrChatServiceBase) {
    var chatService = {
        isInitialized: function () {
            return signalrChatServiceBase.initializationComplete;
        },
        /// Initialize the SignalR Hub.
        initializeService: function (hubName) {
            signalrChatServiceBase.intializeClient(hubName);
            if (!chatService.isInitialized()) {
                chatService.configureClientFunctions();
            }
        },
        configureClientFunctions: function () {
            /// These are like delegates, we are raising an event.
            /// The controllers subscribing to the events will handle the logic
            /// when the response is received.
            signalrChatServiceBase.proxy.on('addNewMessageToPage', function (message) {
                $rootScope.$broadcast('addNewMessageToPageResponse', message);
            });
        },
        sendMessage: function (message) {
            signalrChatServiceBase.proxy.invoke('send', message);
        }
    }
    return chatService;
}]);

app.factory('signalrChatServiceBase', ['$', '$rootScope', function ($, $rootScope) {
    var signalrServiceBase = {
        connection: null,
        proxy: null,
        initializationComplete: false,
        /// Initialize the SignalR Hub.
        intializeClient: function (hubName) {
            console.log('initializing the signalrServiceBase.intializeClient()');
            if (!signalrServiceBase.initializationComplete) {
                signalrServiceBase.connection = $.hubConnection();
                signalrServiceBase.proxy = signalrServiceBase.connection.createHubProxy(hubName);
                signalrServiceBase.start();
            }
            else {
                console.log('The signalr hub initialization is complete.  Raising the complete event.');
                $rootScope.$broadcast("raiseHubInitializationComplete");
            }
        },
        start: function () {
            //starting the connection and initializing market             
            signalrServiceBase.connection.start().done(function () {
                signalrServiceBase.initializationComplete = true;
                console.log('The signalr hub initialization is complete.  Raising the complete event.');
                $rootScope.$broadcast("raiseHubInitializationComplete");
            });
        },
        reset: function () {
            signalrServiceBase.proxy.invoke('reset');
        }
    };
    return signalrServiceBase;
}]);