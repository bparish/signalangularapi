﻿using signalrTest.Hubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace signalrTest.Controllers
{
    public class ChatController : HubController<Chat2Hub>
    {
        public HttpResponseMessage Post(ChatMessage chat)
        { 
            // do something with chat message
            //var onlyMe = Chat2Hub.Connections.GetConnections("bparish");
            //foreach (var me in onlyMe)
            //{
            //    Hub.Clients.Client(me).addNewMessageToPage(chat);
            //}
            Hub.Clients.All.addNewMessageToPage(chat);
            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }

    public class ChatMessage
    {
        public string UserName { get; set; }
        public string Message { get; set; }
    }
}
